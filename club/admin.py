from django.contrib import admin

# Register your models here.
from django.db.models import Q
from django.forms import forms
from django import forms

from club.models import *


@admin.register(Genero)
class GeneroAdmin(admin.ModelAdmin):
    list_display = ('id', 'codigo', 'descripcion')
    ordering = ('id',)


# Linea obligatoria para agregar Dvd al menos 1
class DvdInline(admin.StackedInline):
    model = Dvd
    extra = 0
    min_num = 1
    validate_min = True


@admin.register(Pelicula)
class PeliculaAdmin(admin.ModelAdmin):
    inlines = [DvdInline]
    list_display = ('id', 'titulo', 'director', 'genero_id', 'estreno')
    ordering = ('id',)


@admin.register(Dvd)
class DvdAdmin(admin.ModelAdmin):
    list_display = ('id', 'identificador_dvd', 'pelicula_id', 'disponible')
    ordering = ('id',)


@admin.register(Cliente)
class ClienteAdmin(admin.ModelAdmin):
    list_display = ('id', 'dni', 'nombre', 'apellido', 'direccion', 'numero_telefono', 'correo')
    ordering = ('id',)


class AlquilerForm(forms.ModelForm):
    class Meta:
        model = Alquiler
        fields = ('id', 'nro_recibo', 'cliente_id', 'dni_cliente', 'fecha_alquiler', 'fecha_devolucion', 'precio', 'dvd_id')

    # Valida que exista dni_cliente y si es igual al id de ser ingresado
    def clean_dni_cliente(self):
        dni = self.cleaned_data['dni_cliente']
        try:
            cliente_id = self.cleaned_data['cliente_id'].id
        except AttributeError:
            cliente_id = None

        if dni != '':
            if cliente_id is not None:
                cliente_id_dni = Cliente.objects.get(id=cliente_id).dni
                if dni != cliente_id_dni:
                    raise forms.ValidationError("El DNI ingresado no pertenece al Cliente seleccionado")
            else:
                try:
                    Cliente.objects.get(dni=dni)
                except Cliente.DoesNotExist:
                    raise forms.ValidationError("El DNI ingresado no pertenece a ningun Cliente")
        elif cliente_id is None:
            raise forms.ValidationError("Debe ingresar el Dni cliente o seleccionar el Cliente id")

        return dni

    # Validad la fecha_devolucion sea mayor a fecha_alquiler
    def clean_fecha_devolucion(self):
        fecha_devolucion = self.cleaned_data['fecha_devolucion']
        fecha_alquiler = self.cleaned_data['fecha_alquiler']

        if fecha_devolucion is not None:
            if fecha_devolucion < fecha_alquiler:
                raise forms.ValidationError("La Fecha de Devolucion es menor a la Fecha Alquiler.")
        return fecha_devolucion


@admin.register(Alquiler)
class AlquilerAdmin(admin.ModelAdmin):
    form = AlquilerForm
    list_display = ('id', 'nro_recibo', 'cliente_id', 'dni_cliente', 'fecha_alquiler', 'fecha_devolucion', 'precio', 'dvd_id')
    ordering = ('id',)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "dvd_id":
            if "/change/" in request.path:
                # Filtra si esta disponible y si es el asignado
                parent_id = request.resolver_match.kwargs['object_id']
                dvd_id = None
                try:
                    dvd_id = Alquiler.objects.get(id=parent_id).dvd_id_id
                except Alquiler.DoesNotExist:
                    pass

                kwargs["queryset"] = Dvd.objects.filter(Q(id=dvd_id) | Q(disponible=True))
            else:
                # Filtra si esta disponible el DVD
                kwargs["queryset"] = Dvd.objects.filter(disponible=True)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    # Carga DNI del Cliente o carga el id del Cliente
    def save_model(self, request, obj, form, change):

        if obj.dni_cliente != '' and obj.cliente_id_id is None:
            try:
                obj.cliente_id = Cliente.objects.get(dni=obj.dni_cliente)
            except Cliente.DoesNotExist:
                pass
        if obj.cliente_id_id is not None and obj.dni_cliente == '':
            try:
                obj.dni_cliente = Cliente.objects.get(id=obj.cliente_id_id).dni
            except Cliente.DoesNotExist:
                pass

        super().save_model(request, obj, form, change)
