from rest_framework import serializers
from .models import *


class GeneroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genero
        fields = ('id', 'codigo', 'descripcion')


class DvdSlugRelatedField(serializers.ModelSerializer):
    class Meta:
        model = Dvd
        fields = ('id', 'identificador_dvd', 'disponible')


class PeliculaSerializer(serializers.ModelSerializer):
    genero_id = GeneroSerializer(many=False, read_only=True)

    class Meta:
        model = Pelicula
        fields = ('id', 'titulo', 'director', 'genero_id', 'estreno')


class DvdSerializer(serializers.ModelSerializer):
    pelicula_id = PeliculaSerializer(many=False, read_only=True)

    class Meta:
        model = Dvd
        fields = ('id', 'identificador_dvd', 'pelicula_id', 'disponible')


class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = ('id', 'dni', 'nombre', 'apellido', 'direccion', 'numero_telefono', 'correo')


class DvdDepthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dvd
        fields = ('id', 'identificador_dvd', 'pelicula_id', 'disponible')


class AlquilerSerializer(serializers.ModelSerializer):
    cliente_id = ClienteSerializer(many=False, read_only=True)
    dvd_id = DvdDepthSerializer(many=False, read_only=True)

    class Meta:
        model = Alquiler
        fields = (
            'id', 'nro_recibo', 'cliente_id', 'dni_cliente', 'fecha_alquiler', 'fecha_devolucion', 'precio', 'dvd_id')
