from django.db import transaction
from django.db.models import ProtectedError

# Create your views here.
from rest_framework import permissions, status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .serializers import *


class GeneroViewSet(ModelViewSet):
    queryset = Genero.objects.all()
    serializer_class = GeneroSerializer
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ['get', 'post', 'patch', 'delete']

    def get_queryset(self):

        queryset = Genero.objects.all()

        # Parametros
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)

        codigo = self.request.query_params.get('codigo', None)
        if codigo is not None:
            queryset = queryset.filter(codigo__icontains=codigo)

        descripcion = self.request.query_params.get('descripcion', None)
        if descripcion is not None:
            queryset = queryset.filter(descripcion__icontains=descripcion)

        queryset = queryset.order_by('id')

        return queryset

    def create(self, request, *args, **kwargs):

        # Se valida los datos
        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)

        data = request.data

        results = Genero.objects.create(**data)
        output_serializer = GeneroSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data
        serializer = self.get_serializer(data=data, many=isinstance(data, list), partial=True)
        serializer.is_valid(raise_exception=True)

        # Update de id
        if ("id" in data) and (instance.id != data["id"]):
            # Control de peliculas asignadas
            existePelicula = Pelicula.objects.filter(genero_id=instance.id)
            if existePelicula.exists():
                return Response({"id": ["No se puede cambiar el id porque existen Peliculas asignadas a este genero"]}, status=status.HTTP_400_BAD_REQUEST)
        else:
            data["id"] = instance.id

        Genero.objects.filter(id=instance.id).update(**data)
        results = Genero.objects.get(id=data["id"])
        output_serializer = GeneroSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def destroy(self, request, *args, **kwargs):

        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except ProtectedError:
            return Response({"Error": ["Existen Peliculas asignadas a este genero"]}, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Respuesta": ["Se elimino el Genero"]}, status=status.HTTP_200_OK)


class PeliculaViewSet(ModelViewSet):
    queryset = Pelicula.objects.all()
    serializer_class = PeliculaSerializer
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ['get', 'post', 'patch', 'delete']

    def get_queryset(self):

        queryset = Pelicula.objects.all()

        # Parametros
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)

        titulo = self.request.query_params.get('titulo', None)
        if titulo is not None:
            queryset = queryset.filter(titulo__icontains=titulo)

        director = self.request.query_params.get('director', None)
        if director is not None:
            queryset = queryset.filter(director__icontains=director)

        genero_id = self.request.query_params.get('genero_id', None)
        if genero_id is not None:
            queryset = queryset.filter(genero_id=genero_id)

        genero = self.request.query_params.get('genero', None)
        if genero is not None:
            existeGenero = Genero.objects.filter(codigo__icontains=genero)
            queryset = queryset.filter(genero_id__in=existeGenero)

        estreno = self.request.query_params.get('estreno', None)
        if estreno is not None:
            queryset = queryset.filter(estreno=estreno)

        queryset = queryset.order_by('id')

        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data

        # Se valida los datos
        serializer = self.get_serializer(data=data, many=isinstance(data, list))
        serializer.is_valid(raise_exception=True)

        # Control de existencia del campo genero
        if "genero_id" not in data:
            return Response({"genero_id": ["Este campo es obligatorio."]}, status=status.HTTP_400_BAD_REQUEST)

        # Se controla que exista el genero y se asigana la instancia
        try:
            data["genero_id"] = Genero.objects.get(id=data["genero_id"])
        except Genero.DoesNotExist:
            return Response({"genero_id": ["No existe el Genero"]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de existencia del campo dvds
        if "dvds" not in data:
            return Response({"dvds": ["Este campo es obligatorio."]}, status=status.HTTP_400_BAD_REQUEST)

        dvds = data["dvds"]

        # Control de lista de dvds
        if not isinstance(dvds, list):
            return Response({"dvds": ["El argumento debe ser una lista de Dvd [{}]."]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de existencia de al menos un dvd
        if len(dvds) == 0:
            return Response({"dvds": ["La lista debe tener al menos un elemento."]}, status=status.HTTP_400_BAD_REQUEST)

        # Se validad los datos de los dvd
        serializer = DvdSerializer(data=dvds, many=isinstance(dvds, list))
        serializer.is_valid(raise_exception=True)

        # Se carga los datos de la Pelicula
        pelicula = {}
        pelicula["id"] = data["id"] if "id" in data else None
        pelicula["titulo"] = data["titulo"]
        pelicula["director"] = data["director"] if "director" in data else None
        pelicula["genero_id"] = data["genero_id"]
        pelicula["estreno"] = data["estreno"] if "estreno" in data else None

        # Se crean la pelicula y sus dvd, de ser correctos ambas creaciones
        with transaction.atomic():
            resultsPelicula = Pelicula.objects.create(**pelicula)
            for list_elt in dvds:
                list_elt["pelicula_id"] = resultsPelicula
                Dvd.objects.create(**list_elt)

        output_serializer = PeliculaSerializer(resultsPelicula, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data

        # Se validan los datos
        serializer = self.get_serializer(data=data, many=isinstance(data, list), partial=True)
        serializer.is_valid(raise_exception=True)

        # Update de id
        if "id" in data:
            return Response({"id": ["No se puede cambiar el id porque existen Dvd asignados"]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de existencia de genero
        if "genero_id" in data:
            try:
                Genero.objects.get(id=data["genero_id"])
            except Genero.DoesNotExist:
                return Response({"genero_id": ["No existe el Genero"]}, status=status.HTTP_400_BAD_REQUEST)

        Pelicula.objects.filter(id=instance.id).update(**data)
        results = Pelicula.objects.get(id=instance.id)
        output_serializer = PeliculaSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except:
            return Response({"Error": ["No se pudo borrar la pelicula"]}, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Respuesta": ["Se elimino la Pelicula"]}, status=status.HTTP_200_OK)


class DvdViewSet(ModelViewSet):
    queryset = Dvd.objects.all()
    serializer_class = DvdSerializer
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ['get', 'post', 'patch', 'delete']

    def get_queryset(self):

        queryset = self.queryset

        # Parametros
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)

        identificador_dvd = self.request.query_params.get('identificador_dvd', None)
        if identificador_dvd is not None:
            queryset = queryset.filter(identificador_dvd__icontains=identificador_dvd)

        pelicula_id = self.request.query_params.get('pelicula_id', None)
        if pelicula_id is not None:
            queryset = queryset.filter(pelicula_id=pelicula_id)

        pelicula = self.request.query_params.get('pelicula', None)
        if pelicula is not None:
            peliculatitulo = Pelicula.objects.filter(titulo__icontains=pelicula)
            queryset = queryset.filter(pelicula_id__in=peliculatitulo)

        disponible = self.request.query_params.get('disponible', None)
        if disponible is not None:
            queryset = queryset.filter(disponible__iexact=disponible)

        queryset = queryset.order_by('id')

        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data

        # Se validan los datos
        serializer = self.get_serializer(data=data, many=isinstance(data, list))
        serializer.is_valid(raise_exception=True)

        # Control de pelicula_id ingresada
        if "pelicula_id" not in data:
            return Response({"pelicula_id": ["Este campo es obligatorio."]}, status=status.HTTP_400_BAD_REQUEST)

        # Se controla que exista la pelicula y se asigana la instancia
        try:
            data["pelicula_id"] = Pelicula.objects.get(id=data["pelicula_id"])
        except Genero.DoesNotExist:
            return Response({"pelicula_id": ["No existe la Pelicula"]}, status=status.HTTP_400_BAD_REQUEST)

        results = Dvd.objects.create(**data)
        output_serializer = DvdSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data

        # Se validan los datos
        serializer = self.get_serializer(data=data, many=isinstance(data, list), partial=True)
        serializer.is_valid(raise_exception=True)

        # Update de id
        if ("id" in data) and (instance.id != data["id"]):
            # Control de Alquileres asignados
            existeAlquiler = Alquiler.objects.filter(dvd_id=instance.id)
            if existeAlquiler.exists():
                return Response({"id": ["No se puede cambiar el id porque existen Alquiler asignadas a este Dvd"]}, status=status.HTTP_400_BAD_REQUEST)
        else:
            data["id"] = instance.id

        # Update pelicula_id
        if "pelicula_id" in data:

            # Control existencia de Pelicula
            try:
                Pelicula.objects.get(id=data["pelicula_id"])
            except Pelicula.DoesNotExist:
                return Response({"pelicula_id": ["No existe la Pelicula"]}, status=status.HTTP_400_BAD_REQUEST)

            # Control de al menos un Dvd de la Pelicula
            existeunDvd = Dvd.objects.filter(pelicula_id=instance.pelicula_id_id)
            if existeunDvd.count() <= 1:
                return Response({"pelicula_id": ["Al menos debe existir un Dvd de la Pelicula asociada"]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de identificador_dvd unico
        if ("identificador_dvd" in data) and (instance.identificador_dvd != data["identificador_dvd"]):
            identificador_dvd = Dvd.objects.filter(identificador_dvd=data["identificador_dvd"])
            if identificador_dvd.exists():
                return Response({"identificador_dvd": ["Ya existe el identificador_dvd"]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de disponibilidad de dvd cuando no se realizo la devolucion
        if ("disponible" in data) and (data["disponible"] is True):
            existeAlquilado = Alquiler.objects.filter(dvd_id=instance.id, fecha_devolucion__isnull=True)
            if existeAlquilado.exists():
                return Response({"disponible": ["No puede estar disponible por estar alquilado el dvd sin devolucion"]}, status=status.HTTP_400_BAD_REQUEST)

        Dvd.objects.filter(id=instance.id).update(**data)
        results = Dvd.objects.get(id=data["id"])
        output_serializer = DvdSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def destroy(self, request, *args, **kwargs):

        instance = self.get_object()

        # Control de al menos un Dvd de la Pelicula
        existeunDvd = Dvd.objects.filter(pelicula_id=instance.pelicula_id_id).exclude(id=instance.id)
        if not existeunDvd.exists():
            return Response({"Error": ["Al menos debe existir un Dvd en Pelicula asociada"]}, status=status.HTTP_400_BAD_REQUEST)

        try:
            self.perform_destroy(instance)
        except:
            return Response({"Error": ["No se pudo borrar el Dvd"]}, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Respuesta": ["Se elimino la Dvd"]}, status=status.HTTP_200_OK)


class ClienteViewSet(ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ['get', 'post', 'patch', 'delete']

    def get_queryset(self):

        queryset = self.queryset

        # Parametros
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)

        dni = self.request.query_params.get('dni', None)
        if dni is not None:
            queryset = queryset.filter(dni__icontains=dni)

        nombre = self.request.query_params.get('nombre', None)
        if nombre is not None:
            queryset = queryset.filter(nombre__icontains=nombre)

        apellido = self.request.query_params.get('apellido', None)
        if apellido is not None:
            queryset = queryset.filter(apellido__icontains=apellido)

        direccion = self.request.query_params.get('direccion', None)
        if direccion is not None:
            queryset = queryset.filter(direccion__icontains=direccion)

        numero_telefono = self.request.query_params.get('numero_telefono', None)
        if numero_telefono is not None:
            queryset = queryset.filter(numero_telefono__icontains=numero_telefono)

        correo = self.request.query_params.get('correo', None)
        if correo is not None:
            queryset = queryset.filter(correo__icontains=correo)

        queryset = queryset.order_by('id')

        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data

        serializer = self.get_serializer(data=data, many=isinstance(data, list))
        serializer.is_valid(raise_exception=True)

        results = Cliente.objects.create(**data)
        output_serializer = ClienteSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data
        serializer = self.get_serializer(data=data, many=isinstance(data, list), partial=True)
        serializer.is_valid(raise_exception=True)

        # Update de id
        if ("id" in data) and (instance.id != data["id"]):
            # Control de Alquileres asignadas
            existeAlquiler = Alquiler.objects.filter(cliente_id=instance.id)
            if existeAlquiler.exists():
                return Response({"id": ["No se puede cambiar el id porque existen Alquileres asignados a este cliente"]}, status=status.HTTP_400_BAD_REQUEST)
        else:
            data["id"] = instance.id

        Cliente.objects.filter(id=instance.id).update(**data)
        results = Cliente.objects.get(id=data["id"])
        output_serializer = ClienteSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()
            self.perform_destroy(instance)
        except ProtectedError:
            return Response({"Error": ["Existen Alquileres asignados a este Cliente"]}, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Respuesta": ["Se elimino la Cliente"]}, status=status.HTTP_200_OK)


class AlquilerViewSet(ModelViewSet):
    queryset = Alquiler.objects.all()
    serializer_class = AlquilerSerializer
    permission_classes = (permissions.IsAuthenticated,)
    http_method_names = ['get', 'post', 'patch', 'delete']

    def get_queryset(self):

        queryset = self.queryset

        # Parametros
        id = self.request.query_params.get('id', None)
        if id is not None:
            queryset = queryset.filter(id=id)

        nro_recibo = self.request.query_params.get('nro_recibo', None)
        if nro_recibo is not None:
            queryset = queryset.filter(nro_recibo=nro_recibo)

        cliente_id = self.request.query_params.get('cliente_id', None)
        if cliente_id is not None:
            queryset = queryset.filter(cliente_id=cliente_id)

        dni_cliente = self.request.query_params.get('dni_cliente', None)
        if dni_cliente is not None:
            queryset = queryset.filter(dni_cliente__icontains=dni_cliente)

        fecha_alquiler = self.request.query_params.get('fecha_alquiler', None)
        if fecha_alquiler is not None:
            queryset = queryset.filter(fecha_alquiler__icontains=fecha_alquiler)

        fecha_devolucion = self.request.query_params.get('fecha_devolucion', None)
        if fecha_devolucion is not None:
            queryset = queryset.filter(fecha_devolucion__icontains=fecha_devolucion)

        precio = self.request.query_params.get('precio', None)
        if precio is not None:
            queryset = queryset.filter(precio=precio)

        dvd_id = self.request.query_params.get('dvd_id', None)
        if dvd_id is not None:
            queryset = queryset.filter(dvd_id=dvd_id)

        queryset = queryset.order_by('id')

        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data

        serializer = self.get_serializer(data=data, many=isinstance(data, list))
        serializer.is_valid(raise_exception=True)

        if "cliente_id" in data:
            # Control de existencia de Cliente
            try:
                data["cliente_id"] = Cliente.objects.get(id=data["cliente_id"])
            except Cliente.DoesNotExist:
                return Response({"cliente_id": ["No existe el Cliente"]}, status=status.HTTP_400_BAD_REQUEST)

            # Control igualdad del dni del Cliente y el dni ingresado
            if ("dni_cliente" in data) and data["cliente_id"].dni != data["dni_cliente"]:
                return Response([{"cliente_id": ["El Cliente ingresado no pertenece al Dni"]},
                                 {"dni_cliente": ["El DNI ingresado no pertenece al Cliente"]}], status=status.HTTP_400_BAD_REQUEST)
            # Se asigana el Dni del cliente
            data["dni_cliente"] = data["cliente_id"].dni

        elif "dni_cliente" in data:
            # Control de existencia del dni de Cliente
            try:
                cliente = Cliente.objects.get(dni=data["dni_cliente"])
            except Cliente.DoesNotExist:
                return Response({"dni_cliente": ["No existe el Cliente con el dni ingresado"]}, status=status.HTTP_400_BAD_REQUEST)

            # Se asigna el cliente
            data["cliente_id"] = cliente

        else:
            # Si no ingresa el dni o el cliente
            return Response([{"cliente_id": ["Debe ingresar el cliente_id o el dni_cliente"]},
                             {"dni_cliente": ["Debe ingresar el cliente_id o el dni_cliente"]}], status=status.HTTP_400_BAD_REQUEST)

        # Control de existencia del campo de dvd
        if "dvd_id" not in data:
            return Response({"dvd_id": ["Este campo es obligatorio."]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de existencia y diponiblilidad del dvd
        try:
            data["dvd_id"] = Dvd.objects.get(id=data["dvd_id"], disponible=True)
        except Dvd.DoesNotExist:
            return Response({"dvd_id": ["No existe el Dvd o no esta disponible"]}, status=status.HTTP_400_BAD_REQUEST)


        if "fecha_alquiler" in data:
            # Control de fecha de Alquiler
            fecha_alquiler = datetime.strptime(data["fecha_alquiler"], '%Y-%m-%d').date()
            if fecha_alquiler > date.today():
                return Response({"fecha_alquiler": ["La Fecha de Alquiler no puede ser mayor a la fecha actual."]}, status=status.HTTP_400_BAD_REQUEST)
        else:
            fecha_alquiler = date.today()

        if "fecha_devolucion" in data:
            fecha_devolucion = datetime.strptime(data['fecha_devolucion'],'%Y-%m-%d').date()

            # Control de fecha de devolucion mayor a la fecha de entrega
            if fecha_devolucion < fecha_alquiler:
                return Response({"fecha_devolucion": ["La Fecha de Devolucion debe ser mayor o igual a la Fecha Alquiler."]}, status=status.HTTP_400_BAD_REQUEST)

            # Control de fecha de devolucion mayor a la fecha actual
            if fecha_devolucion > date.today():
                return Response({"fecha_devolucion": ["La Fecha de Devolucion no puede ser mayor a la fecha actual."]}, status=status.HTTP_400_BAD_REQUEST)

        # Se crea el Alquiler y se modifica la disponibilidad a Falso
        with transaction.atomic():
            results = Alquiler.objects.create(**data)
            if "fecha_devolucion" not in data:
                Dvd.objects.filter(id=results.dvd_id_id).update(disponible=False)
                results = Alquiler.objects.get(id=results.id)

        output_serializer = AlquilerSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        data = request.data
        serializer = self.get_serializer(data=data, many=isinstance(data, list), partial=True)
        serializer.is_valid(raise_exception=True)

        if not ("id" in data):
            data["id"] = instance.id

        # Control de cambio de dvd
        if ("dvd_id" in data):
            return Response({"dvd_id": ["No se puede cambiar el dvd, ingrese la fecha_devolucion y vuelva a crear un nuevo alquiler"]}, status=status.HTTP_400_BAD_REQUEST)

        # Control de fecha de alquiler mayor a la fecha actual
        if "fecha_alquiler" in data:
            fecha_alquiler = datetime.strptime(data["fecha_alquiler"], '%Y-%m-%d').date()
            if fecha_alquiler > date.today():
                return Response({"fecha_alquiler": ["La Fecha de Alquiler no puede ser mayor a la fecha actual."]}, status=status.HTTP_400_BAD_REQUEST)
        else:
            fecha_alquiler = instance.fecha_alquiler

        dvdDisponible = False

        if "fecha_devolucion" in data:
            alquilerSinDevolucion = Alquiler.objects.filter(dvd_id=instance.dvd_id_id, fecha_devolucion__isnull=True)

            if data["fecha_devolucion"] is not None:
                fecha_devolucion = datetime.strptime(data['fecha_devolucion'],'%Y-%m-%d').date()

                # Control de fecha de devolucion mayor a la fecha de alquiler
                if fecha_devolucion < fecha_alquiler:
                    return Response({"fecha_devolucion": ["La Fecha de Devolucion debe ser mayor o igual a la Fecha Alquiler."]}, status=status.HTTP_400_BAD_REQUEST)

                # Control de fecha de devolucion mayor a la fecha actual
                if fecha_devolucion > date.today():
                    return Response({"fecha_devolucion": ["La Fecha de Devolucion no puede ser mayor a la fecha actual."]}, status=status.HTTP_400_BAD_REQUEST)

                # Control que no exista otro alquiler sin devolucion para pasar a disponible
                alquilerSinDevolucion = alquilerSinDevolucion.exclude(id=instance.id)
                if not (alquilerSinDevolucion.exists()):
                    dvdDisponible = True

            else:
                if alquilerSinDevolucion.exists() and instance.fecha_devolucion is not None:
                    return Response({"fecha_devolucion": ["No se puede anular la devolucion por que el Dvd esta alquilado y sin devolucion."]}, status=status.HTTP_400_BAD_REQUEST)

        with transaction.atomic():
            Alquiler.objects.filter(id=instance.id).update(**data)
            results = Alquiler.objects.get(id=data["id"])
            if "fecha_devolucion" in data:
                Dvd.objects.filter(id=results.dvd_id_id).update(disponible=dvdDisponible)

        output_serializer = AlquilerSerializer(results, many=False)
        dataout = output_serializer.data

        return Response(dataout)

    def destroy(self, request, *args, **kwargs):
        try:
            instance = self.get_object()

            # Control disponible si no se devolvio el dvd
            if instance.fecha_devolucion is None:
                with transaction.atomic():
                    Dvd.objects.filter(id=instance.dvd_id_id).update(disponible=True)
                    self.perform_destroy(instance)
            else:
                self.perform_destroy(instance)

        except:
            return Response({"Error": ["No se pudo eliminar el Alquiler"]}, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Respuesta": ["Se elimino el Alquiler"]}, status=status.HTTP_200_OK)