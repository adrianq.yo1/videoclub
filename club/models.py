from datetime import datetime, date
from decimal import Decimal

import django
from django.core.validators import MinValueValidator
from django.db import models


class Genero(models.Model):
    id =          models.PositiveIntegerField(primary_key=True, db_column='id', blank=True, null=False, unique=True, help_text='Vacio es autoincremental')
    codigo =      models.CharField(db_column='codigo', max_length=100, null=False, unique=True)
    descripcion = models.CharField(db_column='descripcion', max_length=100, blank=True)

    class Meta:
        db_table = 'Genero'
        verbose_name_plural = 'Generos'
        verbose_name = 'Genero'

    def __str__(self):
        return "{'id': '%s', 'codigo': '%s'}" % (self.id, self.codigo)

    def save(self, *args, **kwargs):
        if self.id is None:
            try:
                self.id = Genero.objects.all().order_by('id').last().id + 1
            except:
                self.id = 1
        super(Genero, self).save(*args, **kwargs)


class Pelicula(models.Model):
    id =        models.PositiveIntegerField(primary_key=True, db_column='id', blank=True, null=False, unique=True, help_text='Vacio es autoincremental')
    titulo =    models.CharField(db_column='codigo', max_length=50, null=False, unique=True)
    director =  models.CharField(db_column='director', max_length=100, blank=True, null=True)
    genero_id = models.ForeignKey(Genero, db_column='genero_id', on_delete=models.PROTECT, null=False, blank=False)
    estreno =   models.DateField(db_column='estreno', null=True, blank=True, help_text='Ejemplo: 01/10/2022')

    class Meta:
        db_table = 'Pelicula'
        verbose_name_plural = 'Peliculas'
        verbose_name = 'Pelicula'

    def __str__(self):
        return "{'id': '%s', 'titulo': '%s'}" % (self.id, self.titulo)

    def save(self, *args, **kwargs):
        if self.id is None:
            try:
                self.id = Pelicula.objects.all().order_by('id').last().id + 1
            except:
                self.id = 1
        super(Pelicula, self).save(*args, **kwargs)


class Dvd(models.Model):
    id =                models.PositiveIntegerField(primary_key=True, db_column='id', blank=True, null=False, unique=True, help_text='Vacio es autoincremental')
    identificador_dvd = models.CharField(db_column='identificador_dvd', max_length=50, null=False, unique=True)
    pelicula_id =       models.ForeignKey(Pelicula, db_column='pelicula_id', on_delete=models.CASCADE, null=False, blank=False)
    disponible =        models.BooleanField(db_column='disponible', null=False, blank=True, default=False)

    class Meta:
        db_table = 'Dvd'
        verbose_name_plural = 'Dvds'
        verbose_name = 'Dvd'

    def __str__(self):
        return "{'id': '%s', 'identificador_dvd': '%s'}" % (self.id, self.identificador_dvd)

    def save(self, *args, **kwargs):
        if self.id is None:
            try:
                self.id = Dvd.objects.all().order_by('id').last().id + 1
            except:
                self.id = 1
        super(Dvd, self).save(*args, **kwargs)


class Cliente(models.Model):
    id =              models.PositiveIntegerField(primary_key=True, db_column='id', blank=True, null=False, unique=True, help_text='Vacio es autoincremental')
    dni =             models.CharField(db_column='dni', max_length=15, null=False, unique=True)
    nombre =          models.CharField(db_column='nombre', max_length=150, null=False)
    apellido =        models.CharField(db_column='apellido', max_length=150, null=False)
    direccion =       models.CharField(db_column='direccion', max_length=255, null=True, blank=True)
    numero_telefono = models.CharField(db_column='numero_telefono', max_length=50, null=True, blank=True)
    correo =          models.EmailField(db_column='correo', max_length=100, null=True, blank=True)

    class Meta:
        db_table = 'Cliente'
        verbose_name_plural = 'Clientes'
        verbose_name = 'Cliente'

    def __str__(self):
        return "{'id': '%s', 'dni': '%s'}" % (self.id, self.dni)

    def save(self, *args, **kwargs):
        if self.id is None:
            try:
                self.id = Cliente.objects.all().order_by('id').last().id + 1
            except:
                self.id = 1
        super(Cliente, self).save(*args, **kwargs)


class Alquiler(models.Model):
    id =               models.PositiveIntegerField(primary_key=True, db_column='id', blank=True, null=False, unique=True, help_text='Vacio es autoincremental')
    nro_recibo =       models.BigIntegerField(db_column='identificador_dvd', null=False, blank=True, unique=True)
    cliente_id =       models.ForeignKey(Cliente, db_column='cliente_id', on_delete=models.PROTECT, null=False, blank=True)
    dni_cliente =      models.CharField(db_column='dni', max_length=15, null=False, blank=True)
    fecha_alquiler =   models.DateField(db_column='fecha_alquiler', null=False, blank=False, help_text='Ejemplo: 01/10/2022', default=django.utils.timezone.now)
    fecha_devolucion = models.DateField(db_column='fecha_devolucion', null=True, blank=True, help_text='Ejemplo: 01/10/2022')
    precio =           models.DecimalField(db_column='precio', max_digits=6, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))], null=False, default=0)
    dvd_id =           models.ForeignKey(Dvd, db_column='dvd_id', on_delete=models.CASCADE, null=False, blank=False)

    class Meta:
        db_table = 'Alquiler'
        verbose_name_plural = 'Alquileres'
        verbose_name = 'Alquiler'

    def __str__(self):
        return "{'id': '%s', 'nro_recibo': '%s'}" % (self.id, self.nro_recibo)

    def save(self, *args, **kwargs):
        if self.id is None:
            try:
                self.id = Alquiler.objects.all().order_by('id').last().id + 1
            except:
                self.id = 1
        if self.nro_recibo is None:
            try:
                self.nro_recibo = Alquiler.objects.all().order_by('nro_recibo').last().nro_recibo + 1
            except:
                self.nro_recibo = 1
        super(Alquiler, self).save(*args, **kwargs)
