from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers

from club.views import *

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'genero', GeneroViewSet)
router.register(r'pelicula', PeliculaViewSet)
router.register(r'dvd', DvdViewSet)
router.register(r'cliente', ClienteViewSet)
router.register(r'alquiler', AlquilerViewSet)

urlpatterns = [
    url(r'^', admin.site.urls),
    url(r'^api/', include(router.urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
